// -*- c++ -*-

// guylib library - some useful tools you might enjoy
//
// Copyright (C) 2011-2016 Guy Bensky
//
// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see
// <http://www.gnu.org/licenses/>.
//
// For more information, bug reports, or to contribute email me at:
// guylib@rabensky.com
//
// or you can look at the git repository at
// https://bitbucket.org/guyben/guylib

#ifndef GUYLIB_FLTK_UPDATER__
#define GUYLIB_FLTK_UPDATER__

#include "guylib.h"

namespace guylib{
	time_counter::updater *fltk_updater(size_t num=3);
}

#endif
