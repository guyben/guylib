// -*- c++ -*-
#ifndef __GNUPLOT_H__
#define __GNUPLOT_H__

#include <vector>
#include <string>

namespace guylib{

	class Gnuplot{
		public:
			Gnuplot(const std::string &header="");
			~Gnuplot();
			void exec(const char *cmd);
			void exec(const std::string &line){exec(line.c_str());}
			Gnuplot &plot(const std::vector<double> &x,
					const std::vector<double> &y,
					const std::string &style="notitle w l");
			Gnuplot &plot(const std::vector<double> &y,
					const std::string &style="notitle w l");
			void saveEps(const std::string &filename);
			void savePng(const std::string &filename, unsigned w, unsigned h);
			void clear(const std::string &limits="[][]");
			void title(const std::string &title);
			void xlabel(const std::string &label);
			void ylabel(const std::string &label);
			void xlog(double base);
			void ylog(double base);
		protected:
			std::string fileHeader;
			std::string limitStr;
			int instanceCounter,lineCounter;
			static int globalInstanceCounter;
			FILE *f;
			//   std::vector<std::string> lines;
	};

}
#endif
