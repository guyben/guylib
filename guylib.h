// -*- c++ -*-

// guylib library - some useful tools you might enjoy
//
// Copyright (C) 2011-2016 Guy Bensky
//
// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see
// <http://www.gnu.org/licenses/>.
//
// For more information, bug reports, or to contribute email me at:
// guylib@rabensky.com
//
// or you can look at the git repository at
// https://bitbucket.org/guyben/guylib

#ifndef GUYLIB_H__
#define GUYLIB_H__

#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <map>
#include <list>
#include <exception>
#include <cstdlib>
#include <sys/time.h>
#include <stdint.h>
#include <iterator>
#include <math.h>

#if defined(__GXX_EXPERIMENTAL_CXX0X) || __cplusplus >= 201103L
#define GUYLIB_CPP_11
#endif

/// @file

/// @brief namespace for the guylib library
///
/// @code
/// DEB; // writes line, file and function location
/// @endcode
namespace guylib{

	/// @brief exception class sent by guylib and the assertErr macro
	///
	/// By default assertErr and guylib exits the program on failure
	/// but when we set assertErr_throw(true) then they throw an 
	/// assertException instead
	/// 

	struct assertException:public std::exception{
		assertException(const std::string &str)throw():msg(str){}
		~assertException()throw(){}
		const char *what()const throw(){return msg.c_str();}
		std::string msg;
	};

	/// @brief internal class used in assertErr and DEB implementation
	struct _assertClass{
		_assertClass(bool iexit):_exit(iexit),first(true){}
		void apply();
		std::ostream &out(){return _out;}
		bool _exit;
		bool first;
		std::ostringstream _out;
		static bool _throw;
		static std::ostream *deb_out;
	};

	/// @brief sets assertErr and guylib to throw on error instead of exiting
	///
	/// By default assertErr and guylib exits the program on failure
	/// but when we set assertErr_throw(true) then they throw an 
	/// assertException instead
	/// 
	inline void assertErr_throw(bool b=true){_assertClass::_throw=b;}
	/// @brief sets the ostream to which DEB, OUT and assertErr write
	///
	/// by default DEB, OUT and assertErr write to std::cerr. This lets you
	/// set a new stream to which they write. Can be used to direct debug
	/// outputs to a log file
	///
	/// note that if assertErr is set to throw - this will have no effect
	/// on it
	///
	/// @param out the new ostream to which to write. NULL means "don't write at all"
	/// @return the previous ostream.
	inline std::ostream *debug_output(std::ostream *out){std::swap(out,_assertClass::deb_out);return out;}
	/// @brief allows you to temporarily set a new ostream output for DEB, OUT and assertErr
	///
	/// like debug_output, but will return to the previous stream at end of scope
	struct debug_output_guard{
		debug_output_guard():aquired(false){}
		debug_output_guard(std::ostream *_out):aquired(false){
			aquire(_out);
		}
		bool aquire(std::ostream *_out){
			if (aquired)
				return false;
			prev=debug_output(_out);
			aquired=true;
			return true;
		}
		bool release(){
			if (!aquired)
				return false;
			debug_output(prev);
			aquired=false;
			return true;
		}
		~debug_output_guard(){release();}
		private:
			std::ostream *prev;
			bool aquired;
	};

/// @brief lets you output the current location to any ostream
#define LINE_FILE __FILE__<<':'<<__LINE__<<": in "<<__PRETTY_FUNCTION__<<": "
/// @brief an assertion "function" that lets you output debug info
///
/// @code
/// assertErr(mymap.count(key)==0)<<"key already appears in mymap!\n"<<OUTPUT(key)<<OUTPUT(mymap[key]);
/// @endcode
/// will print something like that if the assertion fails:
/// @code
/// ==========
/// main.cc:123: in main(int,char**):
/// assertErr(mymap.count(key)==0) failed!
/// key already appears in mymap!
/// key="foo"; mymap[key]= "bar";
/// ==========
/// @endcode
///
/// in addition, if you execute assertErr_throw(true), then assertErr will throw an assertException class
/// with the above text as the what() instead of printing it out and exiting
#define assertErr(x) while(!(x))for(guylib::_assertClass assertErrXXX(true);assertErrXXX.first;assertErrXXX.apply()) assertErrXXX.out()<<LINE_FILE<<" assertErr("<<#x<<") Failed!\n"
/// @brief outputs to std::cerr (or the current selected stream), after adding the location.
///
/// it's convenient to spam DEB; calls when debugging to know where the program crashed.
///
/// Also convenient when debugging to output temporary information - later we'll have a list of locations
/// where we can remove the outputs 
#define DEB for(guylib::_assertClass assertErrXXX(false);assertErrXXX.first;assertErrXXX.apply()) assertErrXXX.out()<<LINE_FILE<<'\n'
/// @brief simply outputs to std::cerr (or the currently selected stream)
#define OUT if (!guylib::_assertClass::deb_out){}else (*guylib::_assertClass::deb_out)
/// @brief creates an std::string of the current file location
#define LOCATION_STR (std::string(__FILE__)+":"+guylib::STR(__LINE__)+": in "+guylib::STR(__PRETTY_FUNCTION__))

	/// @brief holds an ostream state, lets you change the state via textual input
	///
	/// the text can be any combination (in any order) of the following:
	/// - "10" - set width (to 10 in this case). start with 0 (i.e. 010) to set fill to 0
	///   - note that width only remains for next output
	/// - ".5" - set precision (to 5 in this case)
	/// - "+" - write + sign for positive numbers
	/// - "p" - show point for floating-points that have integer value
	/// - "X"/"x" - hex with/without base
	/// - "O"/"o" - oct with/without base
	/// - "D"/"d" - dec
	/// - "b" - show bools as true/false
	/// - "u" - show numerical letters as uppercase (hex letters or the e in floating points etc.)
	/// - "f" - fixed floating points
	/// - "e" - scientific floating points
	/// - "<" - left justification
	/// - ">" - right justification
	/// - "=" - internal justification
	/// - "-*" (where * is any printable character) - set fill to *
	/// @code
	/// cout<<sstate("08X")<<16; // "0x000010
	/// cout<<sstate("+.5f")<<pi; // +3.14159
	/// @endcode
	///
	/// can also record (and later retrieve) the internal stream state.
	/// useful in operator<< implementations where you want to change
	/// the stream state and return it later:
	/// @code
	/// std::ostream &operator<<(std::ostream &out, const myclass &C){
	///   sstream last(out)
	///   // changes to 4 decimal fixed point, prints x,y, returns to previous state
	///   return out << sstate(".4f")<<C.x<<','<<C.y<<last; 
	/// }
	/// @endcode
	///
	/// can also use sstate_guard to return to last state, to be safe
	/// @code
	/// std::ostream &operator<<(std::ostream &out, const myclass &C){
	///   sstream_guard guard(out)
	///   // changes to 4 decimal fixed point, prints x,y, returns to previous state
	///   // is exception safe (stream returns to previous state no matter what)
	///   // and helps if function has many possible exit points
	///   if (C.valid())
	///     return out << sstate(".4f")<<C.x<<','<<C.y;
	///   else
	///     return out << "invalid";
	/// }
	/// @endcode
	///

	struct sstate{
		sstate();
		sstate(const std::string &pat);
		sstate(const char *pat);
		sstate(const std::ostream &s);
		void clear();
		bool from(const char *pat);
		bool from(const std::string &pat){return from(pat.c_str());}
		void from(const std::ios &s);
		void to(std::ios &s)const;
		std::string str()const;
		struct save{
			save(sstate &_s):s(_s){}
			sstate &s;
		};
		std::streamsize precision;
		std::streamsize width;
		std::ios::char_type fill;
		std::ios::fmtflags flags;
	};
	inline std::ostream &operator<<(std::ostream &out,const sstate &s){
		s.to(out);
		return out;
	}
	inline std::ostream &operator<<(std::ostream &out,sstate::save &s){
		s.s.from(out);
		return out;
	}

	/// @brief makes sure to return stream to original state out of scope
	///
	/// @code
	/// std::ostream &operator<<(std::ostream &out, const myclass &C){
	///   sstream_guard guard(out)
	///   // changes to 4 decimal fixed point, prints x,y, returns to previous state
	///   // is exception safe (stream returns to previous state no matter what)
	///   // and helps if function has many possible exit points
	///   if (C.valid())
	///     return out << sstate(".4f")<<C.x<<','<<C.y;
	///   else
	///     return out << "invalid";
	/// }
	/// @endcode
	class sstate_guard{
		public:
			sstate_guard(std::ostream &o):out(o),s(o){}
			~sstate_guard(){s.to(out);}
			std::ostream &out;
		private:
			sstate s;
	};

	/// @brief turns anything into a string
	///
	/// has special implementation for std::vector, std::map, std::pair
	///
	/// uses operator<< for anything else
	template<class T>
	std::string STR(const T&t){
		std::ostringstream out;
		out<<t;
		return out.str();
	}

	/// @brief turns anything into a string, allowing an sstate formatting
	///
	/// has special implementation for std::vector, std::map, std::pair
	///
	/// uses operator<< for anything else
	template<class T>
	std::string STR(const sstate &s, const T&t){
		std::ostringstream out;
		out<<s<<t;
		return out.str();
	}

#define _OUTPUT_CNT(...) _OUTPUT_CNT2(__VA_ARGS__,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0)
#define _OUTPUT_CNT2(a20,a19,a18,a17,a16,a15,a14,a13,a12,a11,a10,a9,a8,a7,a6,a5,a4,a3,a2,a1,a0,...) a0
#define _OUTPUT_RUN(func,...) _OUTPUT_RUN2(func,_OUTPUT_CNT(__VA_ARGS__))
#define _OUTPUT_RUN2(func,n) _OUTPUT_RUN3(func,n)
#define _OUTPUT_RUN3(func,n) func ## n

#define OUTPUT_M(x) #x<<"= "<<guylib::STR(guylib::alt_print(x))<<"; "
#define OUTPUT_1(x) #x<<"= "<<guylib::STR(guylib::alt_print(x))<<';'<<std::endl
#define OUTPUT_2(x,...) OUTPUT_M(x)<<OUTPUT_1(__VA_ARGS__)
#define OUTPUT_3(x,...) OUTPUT_M(x)<<OUTPUT_2(__VA_ARGS__)
#define OUTPUT_4(x,...) OUTPUT_M(x)<<OUTPUT_3(__VA_ARGS__)
#define OUTPUT_5(x,...) OUTPUT_M(x)<<OUTPUT_4(__VA_ARGS__)
#define OUTPUT_6(x,...) OUTPUT_M(x)<<OUTPUT_5(__VA_ARGS__)
#define OUTPUT_7(x,...) OUTPUT_M(x)<<OUTPUT_6(__VA_ARGS__)
#define OUTPUT_8(x,...) OUTPUT_M(x)<<OUTPUT_7(__VA_ARGS__)
#define OUTPUT_9(x,...) OUTPUT_M(x)<<OUTPUT_8(__VA_ARGS__)
#define OUTPUT_10(x,...) OUTPUT_M(x)<<OUTPUT_9(__VA_ARGS__)
#define OUTPUT_11(x,...) OUTPUT_M(x)<<OUTPUT_10(__VA_ARGS__)
#define OUTPUT_12(x,...) OUTPUT_M(x)<<OUTPUT_11(__VA_ARGS__)
#define OUTPUT_13(x,...) OUTPUT_M(x)<<OUTPUT_12(__VA_ARGS__)
#define OUTPUT_14(x,...) OUTPUT_M(x)<<OUTPUT_13(__VA_ARGS__)
#define OUTPUT_15(x,...) OUTPUT_M(x)<<OUTPUT_14(__VA_ARGS__)
#define OUTPUT_16(x,...) OUTPUT_M(x)<<OUTPUT_15(__VA_ARGS__)
#define OUTPUT_17(x,...) OUTPUT_M(x)<<OUTPUT_16(__VA_ARGS__)
#define OUTPUT_18(x,...) OUTPUT_M(x)<<OUTPUT_17(__VA_ARGS__)
#define OUTPUT_19(x,...) OUTPUT_M(x)<<OUTPUT_18(__VA_ARGS__)
/// @brief prints variable=value into a stream
///
/// @code
/// DEB // works with any ostream
///  << OUTPUT(omega,t,cos(omega*t))
///  << OUTPUT(myvec,mymap,mypair)
///  ;
/// @endcode
///
/// prints
/// @code
/// =========
/// main.cc:122: in main(int,char**):
/// omega= 0.5; t= 0.2; cos(omega*t)= 0.9950041652780258;
/// myvec= [1, 2, 3]; mymap= {'1':1, '2':2, '3':3}; mypair= (1,2);
/// @endcode
#define OUTPUT(...) _OUTPUT_RUN(OUTPUT_,__VA_ARGS__)(__VA_ARGS__)

#define OUTPUTF_M(s,x) #x<<"= "<<guylib::STR(s,guylib::alt_print(x))<<"; "
#define OUTPUTF_1(s,x) #x<<"= "<<guylib::STR(s,guylib::alt_print(x))<<';'<<std::endl
#define OUTPUTF_2(s,x,...) OUTPUTF_M(s,x)<<OUTPUTF_1(s,__VA_ARGS__)
#define OUTPUTF_3(s,x,...) OUTPUTF_M(s,x)<<OUTPUTF_2(s,__VA_ARGS__)
#define OUTPUTF_4(s,x,...) OUTPUTF_M(s,x)<<OUTPUTF_3(s,__VA_ARGS__)
#define OUTPUTF_5(s,x,...) OUTPUTF_M(s,x)<<OUTPUTF_4(s,__VA_ARGS__)
#define OUTPUTF_6(s,x,...) OUTPUTF_M(s,x)<<OUTPUTF_5(s,__VA_ARGS__)
#define OUTPUTF_7(s,x,...) OUTPUTF_M(s,x)<<OUTPUTF_6(s,__VA_ARGS__)
#define OUTPUTF_8(s,x,...) OUTPUTF_M(s,x)<<OUTPUTF_7(s,__VA_ARGS__)
#define OUTPUTF_9(s,x,...) OUTPUTF_M(s,x)<<OUTPUTF_8(s,__VA_ARGS__)
#define OUTPUTF_10(s,x,...) OUTPUTF_M(s,x)<<OUTPUTF_9(s,__VA_ARGS__)
#define OUTPUTF_11(s,x,...) OUTPUTF_M(s,x)<<OUTPUTF_10(s,__VA_ARGS__)
#define OUTPUTF_12(s,x,...) OUTPUTF_M(s,x)<<OUTPUTF_11(s,__VA_ARGS__)
#define OUTPUTF_13(s,x,...) OUTPUTF_M(s,x)<<OUTPUTF_12(s,__VA_ARGS__)
#define OUTPUTF_14(s,x,...) OUTPUTF_M(s,x)<<OUTPUTF_13(s,__VA_ARGS__)
#define OUTPUTF_15(s,x,...) OUTPUTF_M(s,x)<<OUTPUTF_14(s,__VA_ARGS__)
#define OUTPUTF_16(s,x,...) OUTPUTF_M(s,x)<<OUTPUTF_15(s,__VA_ARGS__)
#define OUTPUTF_17(s,x,...) OUTPUTF_M(s,x)<<OUTPUTF_16(s,__VA_ARGS__)
#define OUTPUTF_18(s,x,...) OUTPUTF_M(s,x)<<OUTPUTF_17(s,__VA_ARGS__)
#define OUTPUTF_19(s,x,...) OUTPUTF_M(s,x)<<OUTPUTF_18(s,__VA_ARGS__)
#define OUTPUTF(s,...) _OUTPUT_RUN(OUTPUTF_,__VA_ARGS__)(s,__VA_ARGS__)
/// @brief prints variable=value into a stream, allowing a formatting sstream
///
/// @code
/// DEB // works with any ostream
///  << OUTPUT("04X",myvec,mymap,mypair)
///  ;
/// @endcode
///
/// prints
/// @code
/// =========
/// main.cc:122: in main(int,char**):
/// myvec= [0x01, 0x02, 0x03]; mymap= {'1':0x01, '2':0x02, '3':0x03}; mypair= (0x01,0x02);
/// @endcode

#define OUTPUTFF_M(s,x) #x<<"= "<<guylib::STR(s,guylib::alt_print(x))<<"; "
#define OUTPUTFF_2(s,x) #x<<"= "<<guylib::STR(s,guylib::alt_print(x))<<';'<<std::endl
#define OUTPUTFF_4(s,x,...) OUTPUTF_M(s,x)<<OUTPUTFF_2(__VA_ARGS__)
#define OUTPUTFF_6(s,x,...) OUTPUTF_M(s,x)<<OUTPUTFF_4(__VA_ARGS__)
#define OUTPUTFF_8(s,x,...) OUTPUTF_M(s,x)<<OUTPUTFF_6(__VA_ARGS__)
#define OUTPUTFF_10(s,x,...) OUTPUTF_M(s,x)<<OUTPUTFF_8(__VA_ARGS__)
#define OUTPUTFF_12(s,x,...) OUTPUTF_M(s,x)<<OUTPUTFF_10(__VA_ARGS__)
#define OUTPUTFF_14(s,x,...) OUTPUTF_M(s,x)<<OUTPUTFF_12(__VA_ARGS__)
#define OUTPUTFF_16(s,x,...) OUTPUTF_M(s,x)<<OUTPUTFF_14(__VA_ARGS__)
#define OUTPUTFF_18(s,x,...) OUTPUTF_M(s,x)<<OUTPUTFF_16(__VA_ARGS__)
/// @brief prints variable=value into a stream, allowing a formatting sstream per value
///
/// @code
/// DEB // works with any ostream
///  << OUTPUT("04X",myvec,"",mymap,"03x",mypair)
///  ;
/// @endcode
///
/// prints
/// @code
/// =========
/// main.cc:122: in main(int,char**):
/// myvec= [0x01, 0x02, 0x03]; mymap= {'1':1, '2':2, '3':3}; mypair= (001,002);
/// @endcode
#define OUTPUTFF(...) _OUTPUT_RUN(OUTPUTFF_,__VA_ARGS__)(__VA_ARGS__)

	template<class T>
	struct _alt_print{
		_alt_print(const T&_t):t(_t){}
		void print_me(std::ostream &out)const{out<<t;}
		const T&t;
	};

	template<class T>
	_alt_print<T> alt_print(const T&t){return _alt_print<T>(t);}

	template<class T>
	std::ostream &operator<<(std::ostream &out,const _alt_print<T> &a){
		a.print_me(out);
		return out;
	}

	template<class T1, class T2>
	std::ostream &operator<<(std::ostream &out,const std::pair<T1,T2> &p){
		sstate s(out),c;
		return out<<c<<'('<<s<<alt_print(p.first)<<c<<','<<s<<alt_print(p.second)<<c<<')'<<s;
	}

	template<class ITER>
	struct _container_out{
		_container_out(ITER _b, ITER _e, size_t _max, bool _down):b(_b),e(_e),max(_max),down(_down){}
		void print_down(std::ostream &out)const{
			sstate s(out);
			sstate c;
			if (b==e){
				out <<c<< "[empty vector]"<<s;
				return;
			}
			size_t width=0;
			size_t size=std::distance(b,e);
			for(size_t num=size-1;num;num/=10)
				width+=1;
			if (!width) width=1;
			size_t cnt=0;
			for (ITER i=b;i!=e;++i,++cnt){
				if (cnt>=max && cnt!=size-1) continue;
				out<<c<<'\n';
				if (cnt==size-1 && max<cnt) out<<"...\n";
				out.width(width);
				out<<cnt<<": "<<s<<alt_print(*i);
			}
		}
		void print_vec(std::ostream &out)const{
			sstate s(out);
			sstate c;
			if (b==e){
				out << c<<"[]"<<s;
				return;
			}
			size_t size=std::distance(b,e);
			size_t cnt=0;
			out<<c<<'[';
			for (ITER i=b;i!=e;++i,++cnt){
				if (cnt>=max && cnt!=size-1) continue;
				out<<c;
				if (i!=b)
					out<<", ";
				if (cnt==size-1 && max<cnt) out<<"..., ";
				out<<s<<alt_print(*i);
			}
			out<<c<<']'<<s;
		}
		void print_me(std::ostream &out)const{
			if (down)
				print_down(out);
			else
				print_vec(out);
		}
		ITER b,e;
		size_t max;
		bool down;
	};
	template<class ITER>
	std::ostream &operator<<(std::ostream &out, const _container_out<ITER> &c){
		c.print_me(out);
		return out;
	}
	/// @brief use to print an iterator range like a vector
	///
	/// @code
	/// cout << vec_out(argv,argv+argc);// ['./a.out','--help']
	/// @endcode
	///
	/// @param b iterator pointing to the first element
	/// @param e iterator pointing to one-after the last element
	/// @param n (optional) if set - prints at most n elements (n-1 at start, then ..., then one last element)
	template<class ITER>
	_container_out<ITER> vec_out(ITER b, ITER e, size_t n=size_t(-1)){return _container_out<ITER>(b,e,n,false);}
	/// @brief use to print an iterator range like as a line-by-line list
	///
	/// @code
	/// cout << vec_out(argv,argv+argc);
	/// @endcode
	/// will write
	/// @code
	/// 0) ./a.out
	/// 1) --help
	/// @endcode
	///
	/// @param b iterator pointing to the first element
	/// @param e iterator pointing to one-after the last element
	/// @param n (optional) if set - prints at most n elements (n-1 at start, then ..., then one last element)
	template<class ITER>
	_container_out<ITER> vec_down(ITER b, ITER e, size_t n=size_t(-1)){return _container_out<ITER>(b,e,n,true);}
	/// @brief use to print any container (with begin,end) like a vector
	template<class C>
	_container_out<typename C::const_iterator> vec_out(const C&c, size_t n=size_t(-1)){return _container_out<typename C::const_iterator>(c.begin(),c.end(),n,false);}
	/// @brief use to print any container (with begin,end) line-by-line list
	template<class C>
	_container_out<typename C::const_iterator> vec_down(const C&c, size_t n=size_t(-1)){return _container_out<typename C::const_iterator>(c.begin(),c.end(),n,true);}

	template<class T, class A>
	std::ostream &operator<<(std::ostream &out, const std::vector<T,A> &v){return out<<vec_out(v);}

	template<class K, class T, class C, class A>
	std::ostream &operator<<(std::ostream &out, const std::map<K,T,C,A> &v){
		typedef typename std::map<K,T,C,A>::const_iterator ITER;
		ITER b=v.begin();
		ITER e=v.end();
		sstate s(out);
		sstate c;
		if (b==e){
			out << c<<"{}"<<s;
			return out;
		}
		size_t cnt=0;
		out<<c<<'{';
		for (ITER i=b;i!=e;++i,++cnt){
			out<<c;
			if (i!=b)
				out<<", ";
			out<<s<<alt_print(i->first)<<c<<": "<<s<<alt_print(i->second);
		}
		out<<c<<'}'<<s;
		return out;
	}

	/// @brief adds a char to an ostream, escaping it as needed
	///
	/// - if the char isn't printable - will write \x?? (with ?? the hex code of the char)
	/// - if the char is \n, \r, \t, \0 \\ etc., will write that
	/// - if the char is the sep input, will write \[sep]
	void add_encoded_char(std::ostream &out, char c, char sep);

	/// @brief writes a string to an ostream, escaping as needed
	void encode_into(std::ostream &out, const std::string &str, char sep);

	/// @brief returns the excaped string
	std::string encode(const std::string &str, char sep);

	/// @brief turns a hex char into a number 0-15, or -1 if not hex
	int hex2num(char c);

	/// @brief turns an escaped string to the string itself
	std::string decode(const std::string &str);

	/// @brief encode string as lower-case hex
	std::string encode_hex(const std::string &str);
	/// @brief encode string as upper-case hex
	std::string encode_HEX(const std::string &str);
	/// @brief decode a hex string 
	std::string decode_hex(const std::string &str);

	template<>
	void _alt_print<std::string>::print_me(std::ostream &out)const;

	template<>
	void _alt_print<char *>::print_me(std::ostream &out)const;

	template<>
	void _alt_print<const char *>::print_me(std::ostream &out)const;

	//==============================

	/// @brief returns a string of the type name of a type
	template<class T>
	std::string type_name(){
		std::string s= __PRETTY_FUNCTION__;
		size_t a=s.find('=');
		assertErr(a!=std::string::npos && a<s.size()-2 && s[a+1] == ' ')<<OUTPUT(s)<<OUTPUT(a);
		a+=2;
		size_t b=s.find(';',a);
		assertErr(b!=std::string::npos)<<OUTPUT(s)<<OUTPUT(a)<<OUTPUT(b);
		return s.substr(a,b-a);
	}

	/// @brief returns a string of the type name of a variable
	///
	/// use if you want to see what the type of something is when debugging
	///
	template<class T>
	std::string type_name(const T&t){
		return type_name<T>();
	}

	/// @brief use to measure time between points of code
	///
	/// @code
	/// timer T;
	/// do_something(1);
	/// DEB << OUTPUT(timer()-T); // time do_something(1) took
	/// do_something(2);
	/// DEB << OUTPUT(T.reset()); // time do_something(1) and do_something(2) took
	/// // reset also resets the internal clock to now
	/// do_something(3);
	/// DEB << OUTPUT(T.reset()); // time do_something(3) took
	/// @endcode
	class timer{
		public:
			timer(){now();}
			void now(){
				gettimeofday(&tv,NULL);
			}
			double reset(){
				timer t;
				double res=t-(*this);
				*this=t;
				return res;
			}
			double operator-(const timer &o)const{
				return tv.tv_sec-o.tv.tv_sec+(tv.tv_usec-o.tv.tv_usec)*1.e-6;
			}
		private:
			timeval tv;
	};

	class _func_timer{
		public:
			_func_timer(const char *file, int line, const char *function){
				for (size_t i=0;i<depth;++i)
					OUT<<' ';
				out<<"FUNCTION TIMER - "<<file<<':'<<line<<": "<<function;
				OUT<<"> "<<out.str()<<std::endl;
				++depth;
				T.now();
			}
			~_func_timer(){
				double t=T.reset();
				--depth;
				for (size_t i=0;i<depth;++i)
					OUT<<' ';
				OUT<<"< "<<out.str()<<" Took "<<t<<" seconds"<<std::endl;
			}
		private:
			static size_t depth;
			timer T;
			std::ostringstream out;
	};

	/// @brief outputs time between this point and end of scope
	///
	/// will output a "starting measuring <location> message
	/// then at end of scope will print the starting location and time it took
	///
	/// if nested statements exist, they are indented so it's easy to profile
	///
	/// can't use twice in same scope (if you want to use twice, add inner scope)
	/// @code
	/// void F(){
	///   FUNC_TIMER; // will measure do_something(0)/(1)and(2)
	///   do_something(0);
	///   // FUNC_TIMER; // illegal! Same scope as previous one
	///   {
	///     FUNC_TIMER; // will only measure do_something(1)
	///     do_something(1)
	///   }
	///   do_something(2)
	/// }
	/// @endcode
	///
	/// will output
	/// @code
	/// > FUNCTION TIMER - cwttt.cc:40: void F()
	///   > FUNCTION TIMER - ttt.cc:44: void F()
	///   < FUNCTION TIMER - ttt.cc:44: void F() Took 10 seconds
	/// < FUNCTION TIMER - ttt.cc:40: void F() Took 31 seconds
	/// @endcode
#define FUNC_TIMER guylib::_func_timer XXX_FUNC_TIMER_XXX(__FILE__,__LINE__,__PRETTY_FUNCTION__)

	/// @brief converts seconds to days/hours/minutes/second text
	std::string sec2text(uint64_t t);
	/// @brief converts seconds to days/hours/minutes/second/millisecond text
	std::string sec2textf(double t);

	std::string as_logn(double x,unsigned n);
	std::string as_logn(uint64_t x,unsigned n);

	/// @brief simple class that does a proogress printout
	/// 
	/// you give the number of events in the constructor (or 0 if unknown)
	/// and you use operator++ when the events happen
	/// this updates a listner of the progress once every ~second
	class time_counter{
		public:
			time_counter(uint64_t _total=0,const std::string &name="");
			void operator++(){
				++count;
				if (count<estimated) return;
				update_timer();
			}
			void start();
			void done();
			uint64_t get_total()const{return total;}
			void set_total(uint64_t t){total=t;}
		public:
			class updater{
				public:
					virtual size_t init(uint64_t total,const std::string &name)=0;
					virtual void update(size_t id, uint64_t count, uint64_t total, double t)=0;
					virtual void done(size_t id, uint64_t count, double t)=0;
					static std::string text_init(uint64_t total);
					static std::string text_update(uint64_t count, uint64_t total, double t);
					static std::string text_done(uint64_t count, double t);
					static void set_log(unsigned n){logn=n;}
					static unsigned get_log(){return logn;}
				private:
					static unsigned logn;
			};
			static void set_updater(updater *u){updater_p=u;}
			static updater *default_updater();
		private:
			void update_timer();
			uint64_t count,total;
			uint64_t estimated,diff;
			double last_time;
			timer T0;
			std::string name;
			size_t id;
			bool initialized;

		private:
			static updater *updater_p;
	};
	time_counter::updater *console_updater();

	template<class ITER>
	struct progress_iter_t:public std::iterator<
										typename std::forward_iterator_tag,
										typename std::iterator_traits<ITER>::value_type,
										typename std::iterator_traits<ITER>::difference_type,
										typename std::iterator_traits<ITER>::pointer,
										typename std::iterator_traits<ITER>::reference>
	{
		public:
			progress_iter_t(ITER _iter,uint64_t _total=0, const std::string &name=""):iter(_iter),tc(_total,name),do_done(false){tc.start();}
			// used for the "end", doesn't start the counter
			progress_iter_t(ITER _iter,std::input_iterator_tag):iter(_iter),do_done(false){}
			~progress_iter_t(){if(do_done)tc.done();}
			progress_iter_t &operator++(){
				++tc;
				++iter;
				do_done=true;
				return *this;
			}
			progress_iter_t operator++(int){
				progress_iter_t res=*this;
				++(*this);
				return res;
			}
			inline bool operator==(const progress_iter_t &o){return iter==o.iter;}
			inline bool operator!=(const progress_iter_t &o){return iter!=o.iter;}
			friend inline bool operator==(progress_iter_t &a, ITER b){a.setup(b,typename std::iterator_traits<ITER>::iterator_category());return a.iter==b;}
			friend inline bool operator!=(progress_iter_t &a, ITER b){a.setup(b,typename std::iterator_traits<ITER>::iterator_category());return a.iter!=b;}
			friend inline bool operator==(ITER b, progress_iter_t &a){a.setup(b,typename std::iterator_traits<ITER>::iterator_category());return a.iter==b;}
			friend inline bool operator!=(ITER b, progress_iter_t &a){a.setup(b,typename std::iterator_traits<ITER>::iterator_category());return a.iter!=b;}
			typename std::iterator_traits<ITER>::reference operator*(){return *iter;}
			ITER operator->(){return iter;}

		private:
			inline void setup(const ITER &b,std::input_iterator_tag){
			}
			inline void setup(const ITER &b,std::random_access_iterator_tag){
				if (tc.get_total()) return;
				tc.set_total(b-iter);
			}
			ITER iter;
			time_counter tc;
			bool do_done;
	};

	template<class ITER>
	progress_iter_t<ITER> progress_iter(ITER i, const std::string &name="", uint64_t _total=0){return progress_iter_t<ITER>(i,_total,name);}
	template<class ITER>
	progress_iter_t<ITER> progress_end(ITER i){return progress_iter_t<ITER>(i,std::input_iterator_tag());}

	template<class ITER>
	class _range{
		public:
			typedef ITER iterator;
			typedef ITER const_iterator;
			_range(ITER _b, ITER _e):b(_b),e(_e),s(0){}
			ITER begin()const{return b;}
			ITER end()const{return e;}
			typename std::iterator_traits<ITER>::difference_type size()const{if (s==0) return s=std::distance(b,e); return s;}
			template<class C>
			operator C()const{return C(begin(),end());}
		private:
			ITER b,e;
			mutable typename std::iterator_traits<ITER>::difference_type s;
	};

	template<class ITER>
	inline _range<ITER> make_range(ITER b, ITER e){return _range<ITER>(b,e);}
	template<class ITER>
	inline _range<ITER> make_range(std::pair<ITER,ITER> p){return _range<ITER>(p.first,p.second);}

	template<class ITER>
	_range<progress_iter_t<ITER> > progress(ITER b, ITER e,uint64_t size,const std::string &name=""){return make_range(progress_iter(b,name,size),progress_end(e));}
	template<class ITER>
	_range<progress_iter_t<ITER> > progress(ITER b, ITER e,const std::string &name,std::input_iterator_tag){return progress(b,e,0,name);}
	template<class ITER>
	_range<progress_iter_t<ITER> > progress(ITER b, ITER e,const std::string &name,std::random_access_iterator_tag){return progress(b,e,e-b,name);}
	template<class ITER>
	_range<progress_iter_t<ITER> > progress(ITER b, ITER e,const std::string &name=""){return progress(b,e,name,typename std::iterator_traits<ITER>::iterator_category());}

	template<class C>
	_range<progress_iter_t<typename C::const_iterator> > progress(const C&c,const std::string &name=""){
		return progress(c.begin(),c.end(),c.size(),name);
	}
	template<class C>
	_range<progress_iter_t<typename C::iterator> > progress(C&c,const std::string &name=""){
		return progress(c.begin(),c.end(),c.size(),name);
	}

	template<class NUM>
	class progress_num{
		public:
			progress_num(NUM _n,const std::string &name=""):n(_n),tc(0,name){tc.start();}
			~progress_num(){tc.done();}
			NUM operator++(){++n;++tc;return n;}
			NUM operator++(int){NUM r=n;++n;++tc;return r;}
			inline operator NUM()const{return n;}
			inline NUM num()const{return n;}

			template<class N> inline friend bool operator==(progress_num &a, const N &b){a.setup(b);return a.n==b;}
			template<class N> inline friend bool operator!=(progress_num &a, const N &b){a.setup(b);return a.n!=b;}
			template<class N> inline friend bool operator<=(progress_num &a, const N &b){a.setup(b);return a.n<=b;}
			template<class N> inline friend bool operator>=(progress_num &a, const N &b){a.setup(b);return a.n>=b;}
			template<class N> inline friend bool operator< (progress_num &a, const N &b){a.setup(b);return a.n< b;}
			template<class N> inline friend bool operator> (progress_num &a, const N &b){a.setup(b);return a.n> b;}
			template<class N> inline friend bool operator==(const N &b, progress_num &a){a.setup(b);return a.n==b;}
			template<class N> inline friend bool operator!=(const N &b, progress_num &a){a.setup(b);return a.n!=b;}
			template<class N> inline friend bool operator<=(const N &b, progress_num &a){a.setup(b);return a.n<=b;}
			template<class N> inline friend bool operator>=(const N &b, progress_num &a){a.setup(b);return a.n>=b;}
			template<class N> inline friend bool operator< (const N &b, progress_num &a){a.setup(b);return a.n< b;}
			template<class N> inline friend bool operator> (const N &b, progress_num &a){a.setup(b);return a.n> b;}
		private:
			inline void setup(const NUM &b){
				if (tc.get_total()) return;
				if (n<b)
					tc.set_total(b-n);
				else
					tc.set_total(n-b);
			}
			NUM n;
			time_counter tc;
	};

	inline void progress_updater(time_counter::updater *u){time_counter::set_updater(u);}
	inline void progress_logn(unsigned n){time_counter::updater::set_log(n);}

	template<class NUM>
	struct increase_f{inline void operator()(NUM &n){++n;}};

	template<class NUM, class FUNC=increase_f<NUM> >
	struct _as_iter:public std::iterator<
										typename std::forward_iterator_tag,
										NUM,
										int64_t,
										const NUM*,
										const NUM&>
	{
		public:
			_as_iter(NUM n=NUM(),FUNC f=FUNC()):num(n),func(f){}
			_as_iter &operator++(){
				func(num);
				return *this;
			}
			_as_iter operator++(int){
				_as_iter res=*this;
				++(*this);
				return res;
			}
			inline bool operator==(const _as_iter &o)const{return num==o.num;}
			inline bool operator!=(const _as_iter &o)const{return num!=o.num;}
			const NUM &operator*()const{return num;}
			const NUM *operator->()const{return &num;}

		private:
			NUM num;
			FUNC func;
	};

	template<class NUM>
	_as_iter<NUM> make_iter(NUM n){return _as_iter<NUM>(n);}
	template<class NUM,class FUNC>
	_as_iter<NUM,FUNC> make_iter_f(NUM n,FUNC f){return _as_iter<NUM,FUNC>(n,f);}
	
	template<class NUM>
	_range<_as_iter<NUM> > range(NUM a, NUM b){return make_range(make_iter(a),make_iter((a<b)?b:a));}
	template<class NUM>
	_range<_as_iter<NUM> > range(NUM b){return range(NUM(0),b);}
	// template<class NUM,DIFF>
	// _range<_as_iter<NUM,increase_n_f<NUM,DIFF> > > range(NUM a, NUM b, DIFF d){b-=a;b/=d;b=(b>0)b:0;b*=d;b+=a;return make_range(make_iter(a,d),make_iter(b,d));}
	template<class NUM,class FUNC>
	_range<_as_iter<NUM,FUNC> > range_f(NUM a, NUM b, FUNC f){return make_range(make_iter(a,f),make_iter(b,f));}


	//--------------------------------

	template<class ITER>
	std::string join(ITER b, ITER e, const std::string &sep="",sstate s=sstate()){
		std::ostringstream res;
		sstate c;
		for (ITER i=b;i!=e;++i){
			if (i!=b)
				res<<c<<sep;
			res<<s<<*i;
		}
		return res.str();
	}

	template<class C>
	std::string join(const C&c, const std::string &sep="",sstate s=sstate()){
		return join(c.begin(),c.end(),sep,s);
	}

	template<class OITER>
	void split_iter(OITER res, const std::string &str,const std::string &sep=""){
		std::string::size_type loc=0;
		if (sep.empty())
			loc=str.find_first_not_of(" \t\n\r",loc);
		while(loc<str.size()){
			std::string::size_type next;
			if (sep.empty())
				next=str.find_first_of(" \t\n\r",loc);
			else
				next=str.find(sep,loc);
			if (next==std::string::npos)
				next=str.size();
			*res=str.substr(loc,next-loc);
			loc=next;
			if (sep.empty())
				loc=str.find_first_not_of(" \t\n\r",loc);
			else
				loc+=sep.size();
		}
	}

	inline std::vector<std::string> split(const std::string &str, char sep){
		std::vector<std::string> res;
		split_iter(std::back_inserter(res),str,std::string(1,sep));
		return res;
	}

	inline std::vector<std::string> split(const std::string &str, const std::string &sep=""){
		std::vector<std::string> res;
		split_iter(std::back_inserter(res),str,sep);
		return res;
	}

	//-------------------

	template<typename NUM,typename CHECK,typename ADVANCE>
	class _generator_iter:public std::iterator<
										typename std::forward_iterator_tag,
										NUM,
										int64_t,
										const NUM*,
										const NUM&>
	{
		public:
			_generator_iter(NUM n, CHECK c, ADVANCE a):_num(n),_chk(c),_adv(a),_done(!_chk(n)){}
			_generator_iter():_done(true){}
			bool operator==(const _generator_iter &o){return _done && o._done;}
			bool operator!=(const _generator_iter &o){return !(*this==o);}
			const NUM &operator*()const{return _num;}
			const NUM *operator->()const{return &_num;}
			_generator_iter &operator++(){_adv(_num);_done=!_chk(_num);return *this;}
			_generator_iter operator++(int){_generator_iter res=*this;++(*this);return res;}
			_generator_iter end()const{_generator_iter res=*this;res._done=true;return res;}
			bool done()const{return _done;}
		private:
			NUM _num;
			CHECK _chk;
			ADVANCE _adv;
			bool _done;
	};

	template<typename NUM,typename CHECK,typename ADVANCE>
	_range<_generator_iter<NUM,CHECK,ADVANCE> > generator(NUM n, CHECK c, ADVANCE a){
		_generator_iter<NUM,CHECK,ADVANCE> b(n,c,a);
		return make_range(b,b.end());
	}

	//-------------------

	class Glob{
		public:
			typedef const char *const* iterator;
			typedef const char *const* const_iterator;
		Glob(const char *pattern);
		~Glob();
		const_iterator begin()const;
		const_iterator end()const;
		size_t size()const;
		private:
		struct internal_t;
		internal_t *data;
	};

	inline std::ostream &operator<<(std::ostream &out,const Glob &g){
		return out<<guylib::vec_out(g.begin(),g.end());
	}

	std::vector<std::string> glob(const char *pattern);
	inline std::vector<std::string> glob(const std::string &pattern){return glob(pattern.c_str());}

	template<class T>
	inline T sqr(const T &t){return t*t;}

#ifdef GUYLIB_CPP_11
	template <class A, class R>
	class _guard{
		public:
			_guard(A a, R r):_r(r),_aquired(true){a();}
			~_guard(){release();}
			_guard(_guard &&o):_r(std::move(o._r)),_aquired(o._aquired){o._aquired=false;}
			void release(){if (_aquired) _r();_aquired=false;}
			bool aquired()const{return _aquired;}
		private:
			_guard(const _guard &)=delete;
			void operator=(const _guard &)=delete;
			void operator=(_guard &&o)=delete;
			R _r;
			bool _aquired;
	};

	template <class A, class R>
	_guard<A,R> guard(A a, R r){return _guard<A,R>(a,r);}
#endif
}

#endif

