all: libguylib.a libguylib_fltk.a

nox: libguylib.a


Gnuplot.o: Gnuplot.cc Gnuplot.h guylib.h
	g++ -O3 -Wall Gnuplot.cc -c -o Gnuplot.o

fltk_updater.o: fltk_updater.cc fltk_updater.h guylib.h
	g++ -O3 -Wall fltk_updater.cc -c -o fltk_updater.o

guylib.o: guylib.cc guylib.h
	g++ -O3 -Wall guylib.cc -c -o guylib.o

libguylib.a: Gnuplot.o guylib.o
	ar rcs libguylib.a $^
	
libguylib_fltk.a: fltk_updater.o Gnuplot.o guylib.o
	ar rcs libguylib_fltk.a $^
	

install_head: guylib.h fltk_updater.h Gnuplot.h
	mkdir -p /usr/include/guylib
	cp $^ /usr/include/guylib/

install_lib: libguylib.a libguylib_fltk.a
	cp $^ /usr/lib/

install: install_head install_lib

install_nox_head: guylib.h Gnuplot.h
	mkdir -p /usr/include/guylib
	cp $^ /usr/include/guylib/

install_nox_lib: libguylib.a
	cp $^ /usr/lib/

install_nox: install_nox_head install_nox_lib

