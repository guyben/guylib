#include "guylib.h"

int main(int argc, char **argv){
	using namespace guylib;
	if (argc<=1){
		DEB
			<<OUTPUT(Glob("./*.cc"))
			<<OUTPUT(Glob("../*.{cc,h}"))
			<<OUTPUT(vec_down(Glob("{..,.}/{*.{cc,h},Makefile*}")))
			;
	}else{
		for (char *p:guylib::make_range(argv+1,argv+argc))
			std::cout << OUTPUT(p,Glob(p));
	}
}

