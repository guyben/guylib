#include "guylib.h"

int main(int argc, char **argv){
	double tau=2*M_PI;
	DEB; // you can use empty DEB; to track when your program arrives to this line. Instead of cout<<"here"<<endl;
	std::vector<double> X(guylib::make_iter(0),guylib::make_iter(100)),tmp;
	std::map<std::string,std::vector<double> > Y;
	DEB;
	for (auto &x:X) x*=tau/X.size();
	{
		auto &tmp=Y["sin"];
		tmp=X;
		for (auto &y:tmp) y=sin(y);
	}
	DEB;
	{
		auto &tmp=Y["cos"];
		tmp=X;
		for (auto &y:tmp) y=cos(y);
	}
	std::string song;
	song+="There once was a man from Nantucket\n";
	song+="Who kept all his cash in a bucket.\n";

	size_t i=42;

	DEB
		<<"Some debug printouts to see what's the general value of stuff:\n"
		<<OUTPUT(X.size(),X[0],X.back(),X.back()/tau)
		<<OUTPUT(Y.size(),Y.count("sin"),Y.count("cos"),Y.count("tan"))
		<<OUTPUTF("b",Y["sin"].size()==X.size(),Y["cos"].size()==X.size()) // "b" format means std::ios::boolalpha (so bool->true/false)
		<<OUTPUT(i,X[i]/tau,Y["sin"][i],Y["cos"][i])
		;
	std::string title="Data from "+guylib::join(argv,argv+argc," ");
	DEB
		<<"Just paste the following lines into python\n"
		<<"to have these variable there:\n"
		<<OUTPUT(title,song)
		<<OUTPUT(X,Y)
		;
	int a=31;
	int b=27;
	DEB
		<<"Displaying the different kind of OUTPUT*\n\n"
		<<"OUTPUT(a,b,...) - prints a,b,... in default format:\n"
		<<OUTPUT(a,b)
		<<"OUTPUTF(format,a,b,...) - prints a,b,... in the given format:\n"
		<<OUTPUTF("12",a,b)
		<<OUTPUTF("12-_",a,b)
		<<OUTPUTF("012",a,b)
		<<"OUTPUTFF(format,a,format,b,...) - prints a,b,... each with it's own format\n"
		<<"in hex\n"
		<<OUTPUTFF("x",a,"X",a,"06x",a,"06xu",a,"06X",a,"06X=",a,"06Xu=",a)
		<<"in oct\n"
		<<OUTPUTFF("o",a,"O",a,"06o",a,"06ou",a,"06O",a,"06O=",a,"06Ou=",a)
		<<"justify, showpos, precision\n"
		<<OUTPUTFF("12<",-a,"12>",-a,"12=",-a,"+",-a,"+",a,"6.4",tau,"6.4f",tau,"6.4e",tau,"6.4eu",tau)
		;
	return 0;
}

