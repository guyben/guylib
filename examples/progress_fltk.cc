#include "guylib.h"
#include "fltk_updater.h"
#include <algorithm>
#include <stdlib.h>

double heavy(int i){
	double res=0.;
	for (int j=0;j<1<<13;++j)
		res+=sin(i+j);
	return res;
}


int main(){
	using namespace guylib;
	progress_updater(fltk_updater(2));
	double t=0;
	const int N=50000;
 	std::vector<int> v(make_iter(0),make_iter(N));

	// Progress bar without a name:
 	for (progress_num<int> i=0;i<v.size();++i) // write progress_num<int> i(0,"blah") to give it a name
 		t+=heavy(i);

	// nested support:
	for (auto i:progress(range(3),"outer"))
		for (auto j:progress(v,"inner"))
			t+=heavy(i+j);

 	// multiple concurrent support:
	std::vector<decltype(progress_iter(v.begin()))> iters;
	auto i1=progress_iter(v.begin(),"i1");
	auto i2=progress_iter(v.begin(),"i2");
	while (i1!=v.end() && i2!=v.end()){
		t+=heavy(*i1+*i2);
		if (rand()&1)
			++i1;
		else
			++i2;
	}
	DEB<<OUTPUT(t);
	return 0;
}
