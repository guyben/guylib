#include "Gnuplot.h"
#include "guylib.h"

using namespace guylib;

int main(){
	const size_t N=1000;
	std::vector<double> X(N),S(N),C(N),AT(N);
	for (size_t i=0;i<N;++i){
		X[i]=i/100.;
		S[i]=sin(X[i]);
		C[i]=cos(X[i]);
		AT[i]=atan(X[i]);
	}
	Gnuplot plt;
	plt.plot(X,S,"w l title 'sin'");
	plt.plot(X,C,"w l title 'cos'");
	plt.plot(X,AT,"w l title 'atan'");
	plt.xlabel("angle [rad]");
	plt.ylabel("value");
	plt.title("Some trigonometric functions");
	plt.savePng("test_png",640,480);
	plt.saveEps("test_eps");
	return 0;
}

