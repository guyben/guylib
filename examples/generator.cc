#include <guylib.h>


int main(){
	std::vector<double> vec1 = guylib::generator(1,[](int i){return i<21;},[](int &i){++i;});
	std::vector<double> vec2 = guylib::generator(1.,[](double i){return i<21;},[](double &i){i+=0.1;});
	DEB
		<<OUTPUT(vec1)
		<<OUTPUT(vec2)
		;
}

