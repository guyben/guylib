#include "guylib.h"
#include <algorithm>
#include <stdlib.h>

double heavy(int i){
	double res=0.;
	for (int j=0;j<1<<13;++j)
		res+=sin(i+j);
	return res;
}


int main(){
	using namespace guylib;
	double t=0;
	const int N=50000;
 	std::vector<int> v(make_iter(0),make_iter(N));

	DEB<<"Progress bar without a name:";
 	for (progress_num<int> i=0;i<v.size();++i) // write progress_num<int> i(0,"blah") to give it a name
 		t+=heavy(i);

	DEB<<"Progress bar with location as name:";
 	for (auto i=progress_iter(v.begin(),LOCATION_STR);i!=v.end();++i)
 		t+=heavy(*i);

	DEB<<"showing iterations in various bases:";
	progress_logn(2);
 	for (auto i:progress(v,"base 2"))
 		t+=heavy(i);
	progress_logn(10);
 	for (auto i:progress(v,"base 10"))
 		t+=heavy(i);
	progress_logn(1);
 	for (auto i:progress(v,"regular"))
 		t+=heavy(i);
 	// race between 2:
	DEB<<"Need to output t otherwise the compiler will outsmart us :) "<<OUTPUT(t);
	return 0;
}
