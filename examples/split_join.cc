#include "guylib.h"

int main(){
	using namespace guylib;
	DEB
		<<OUTPUT(split("testing: 1,  2,  3 "))
		<<OUTPUT(split("split(str) splits by words, so multiple     whites, still don't return empty strings"))
		<<OUTPUT(split("split(str,\" \") will split by single space, beware of double  spaces"," "))
		<<OUTPUT(split("You can also [hic] split by a word [hic] or string [hic] if you want","[hic]"))
		;
	DEB
		<<OUTPUT(join(split("testing: 1, 2, 3"),"___"))
		<<OUTPUT(join(std::vector<int>{1,2,3},", "))
		<<OUTPUT(join(std::vector<int>{1,2,3},"_","04"))
		;
}

