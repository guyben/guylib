#include <stdio.h>
#include <math.h>
#include <cstring>
#include <iostream>
#include <sstream>
#include <fstream>
#include <unistd.h>
#include "Gnuplot.h"
#include "guylib.h"

namespace guylib{

	using namespace std;

	int Gnuplot::globalInstanceCounter=0;

	Gnuplot::Gnuplot(const std::string &header){
		instanceCounter=globalInstanceCounter;
		globalInstanceCounter++;
		lineCounter=0;
		f=NULL;
		//   const char *initialLimitStr="[][]";
		limitStr="[][]";
		if (header.size()!=0)
			fileHeader=header;
		else
			fileHeader=STR("/tmp/gnuplot_data_")+STR(getpid())+STR("_")+STR(instanceCounter);
		f=popen("gnuplot -persist","w");
		//  exec("set key off");
		exec("set termoption dash");
	}

	Gnuplot::~Gnuplot(){
		if (f!=NULL)
			pclose(f);
	}

	void Gnuplot::clear(const std::string &limits){
		lineCounter=0;
		limitStr=limits;
	}
	void Gnuplot::exec(const char *cmd){
		if (f==NULL || cmd==NULL){
			return;
		}
		for (const char *c=cmd;*c;++c){
			fputc(*c,f);
			//if (*c=='\\')
			//	fputc(*c,f);
		}
		fputc('\n',f);
		fflush(f);
	}

	void Gnuplot::xlog(double base){
		if (base>1)
			exec("set logscale x "+STR(base));
		else
			exec("unset logscale x");
		if (lineCounter>0)
			exec("replot");
	}
	void Gnuplot::ylog(double base){
		if (base>1)
			exec("set logscale y "+STR(base));
		else
			exec("unset logscale y");
		if (lineCounter>0)
			exec("replot");
	}
	void Gnuplot::xlabel(const std::string &label){
		exec("set xlabel '"+label+"'");
		if (lineCounter>0)
			exec("replot");
	}
	void Gnuplot::ylabel(const std::string &label){
		exec("set ylabel '"+label+"'");
		if (lineCounter>0)
			exec("replot");
	}
	void Gnuplot::title(const std::string &title){
		exec("set title '"+title+"'");
		if (lineCounter>0)
			exec("replot");
	}

	Gnuplot &Gnuplot::plot(const std::vector<double> &x,
			const std::vector<double> &y,
			const std::string &style){
		size_t size=std::min(x.size(),y.size());
		ostringstream fileName;
		fileName<<fileHeader<<"_"<<lineCounter<<".dat";
		ofstream f(fileName.str().c_str());
		f<<sstate(".15");
		for(uint i=0;i<size;++i)
			f<<x[i]<<" \t "<<y[i]<<endl;
		ostringstream line;
		if (lineCounter==0)
			line<<"plot "<<limitStr;
		else
			line<<"replot";
		line<<" '"<<fileName.str()<<"' u 1:2 "<<style;
		exec(line.str());
		lineCounter++;
		return *this;
	}

	Gnuplot &Gnuplot::plot(const std::vector<double> &y,
			const std::string &style){
		return plot(std::vector<double>(make_iter(size_t(0)),make_iter(y.size())),y,style);
	}

	void Gnuplot::savePng(const std::string &filename, unsigned w, unsigned h){
		exec("set terminal push");
		exec("set terminal png size "+STR(w)+","+STR(h)+" enhanced");
		exec(STR("set output '")+filename+STR(".png'"));
		exec("replot");
		exec("set terminal pop");
		exec("replot");
	}

	void Gnuplot::saveEps(const std::string &filename){
		exec("set terminal push");
		exec("set terminal postscript eps enhanced color");
		exec(STR("set output '")+filename+STR(".eps'"));
		exec("replot");
		exec("set terminal pop");
		exec("replot");
	}

}


